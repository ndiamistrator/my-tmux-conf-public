#!/bin/bash

# TODO: mode
# TODO: rebind

set -e

exec >>"$0.log" 2>&1 <&-
echo "::: ${@@Q}"
echo "::: ${@@Q}" >> "$0.calls"
# exit

tmux="$(realpath "$(dirname "$0")")"

########################################

. "$(realpath "$(dirname "$0")")/my-atexit.bash/atexit.bash"

########################################

unset func; declare -A func

########################################

# func[noop]=1
# noop(){
#     return
# }

########################################

func[init]=1
init(){
    [[ "$TMUX" =~ ^(.*),[^,]+,[^,]+$ ]] || exit 1
    sock=$(realpath "${BASH_REMATCH[1]}")
    name=${sock#/tmp/tmux-$(id -u)/}
    if [ "$name" = "$sock" ]; then
        tmux set -g @status-socket "-S $sock"
    elif [ "$name" = 'default' ]; then
        tmux set -g @status-socket ''
    else
        tmux set -g @status-socket "-L $name "
    fi
    tmux set -g @tmux "$tmux"

    tmux source "$tmux/options.tmux"
    tmux source "$tmux/keys.tmux"
    tmux source "$tmux/status.tmux"

    tmux source "$tmux/nomodal.tmux"
    #tmux source "$tmux/modal.tmux"
    #mode-rebind

    tmux source "$tmux/hooks.tmux"
}

_reset_escape() {
    awk 'index("$\"'\''", $4) {$4="\\" $4} {print}'
    # awk 'index("$\"'\''", $4) {$4="\\" $4; next} {print}'
}

func[_reset]=1
_reset(){
    local xrm= tmp=$(mktemp -d)
    atexit -v xrm rm -rf "$tmp"

    tmux -S "$tmp/tmux" -f /dev/null new-session -d
    local xtmux=
    atexit -v xtmux tmux -S "$tmp/tmux" kill-server

    (   tmux -S "$tmp/tmux" show -gp | awk 'NF>1 {print "set -gp", $0}'
        tmux -S "$tmp/tmux" show -gw | awk 'NF>1 {print "set -gw", $0}'
        tmux -S "$tmp/tmux" show -gs | awk 'NF>1 {print "set -gs", $0}'
        tmux -S "$tmp/tmux" show -g  | awk 'NF>1 {print "set -g", $0}'
        tmux -S "$tmp/tmux" list-keys | _reset_escape
    ) | (egrep -v '^set -g.? \S+ Invalid#1fff0+$'||true) >> "$tmp/reset"

    nowatexit $xtmux

    tmux show-hooks -g | awk '{print "set-hook -ug", $1}' >> "$tmp/clear-hooks"

    (   tmux show -gp | awk '{print "set -ugp", $1}'
        tmux show -gw | awk '{print "set -ugw", $1}'
        tmux show -gs | awk '{print "set -ugs", $1}'
        tmux show -g  | awk '{print "set -ug", $1}'
        tmux list-keys | sed -re 's/^bind-key(\s+-r)?(\s+-T\s+\S+)\s+(\S+).*/unbind\2 \3/' \
            | _reset_escape
    ) >> "$tmp/clear"

    tmux set-hook -gu after-set-hook
    tmux source "$tmp/clear-hooks"
    tmux source "$tmp/clear"
    tmux source "$tmp/reset"

    nowatexit $xrm
}

func[reset]=1
reset(){
    _reset
    init
}

# ARG: #{q:@mode-rebind}
func[mode-rebind]=1
mode-rebind(){
    local DELAY=1
    local mode_rebind=$1

    if [ $((mode_rebind+0)) -gt 0 ]; then
        echo "corner case (@mode-rebind=$mode_rebind)" >&2
        tmux set-hook -gu after-bind-key \; set -gu @mode-rebind
        exit 1
    fi

    tmux set -g @mode-rebind 1
    local abk=$(tmux show-hook -g after-bind-key 2>/dev/null || true)
    tmux set-hook -gu after-bind-key

    (
        sleep "$DELAY"
        local xrm= tmp=$(mktemp)
        atexit -v xrm rm -rf "$tmp"
        tmux list-keys \
        | perl -n -e 'BEGIN {
            $match = "rebound-mode-out";
            $prefix = "run-shell \"#{q:\@tmux}/sh rebound-mode-out\"";
            sub process {if ($2 !~ /$match/) {printf "%s%s \\; %s%s\n", $1, $prefix, $2, $3;}}
        } {
            s/^(bind-key\s+-T\s+prefix\s+\S+\s+)(.*?(?<!\\)(?:\\\\)*)((?:\\;.*?)?)$/process/ge;
        }' > "$tmp"
        # | awk -vadd='run-shell "#{q:@tmux}/sh mode-out"' '
        #     /^bind-key\s+-T\s+prefix\s/ {
        #         key=$4; $1=$2=$3=$4="";
        #         if (index($0, add) != 4) {
        #             print "bind -T prefix", key, add "\"\\;", $0;
        # }}' > "$tmp"
        tmux source "$tmp"
        nowatexit $xrm || true
        mode_rebind=$(tmux show -Av @mode-rebind)
        if [ ! "$abk" ] || [ "$abk" = 'after-bind-key' ]; then
            tmux set -gu @mode-rebind
        else
            perl -n -e 'BEGIN {
                print "set -gu \@mode-rebind\n";
            } {
                s/^(\S+) (.*)$/set-hook -g $1 {\n$2\n}/ && print;
            }' <<<"$abk" | tmux source -
        fi
    ) </dev/null &
}

# starttime() {
#     perl -p -e 's/^.*\)(?:\s+\S+){19}\s+(\d+).*/$1/' "/proc/$1/stat" 2>/dev/null || true
# }

func[mode-in]=1
mode-in(){
    tmux source - <<'EOF'
        %if "#{!=:#{@mode},1}"
            set -F    @mode-window      "#{window_id}"
            set -F    @mode-window-last "#{W:#{?window_last_flag,#{window_id},}}"
            #et -F -w @mode-pane        "#{pane_id}"
        %endif
        set @mode 1
        set key-table prefix
        set -F status-style "#{@status-in-style}"
        #set -g status off
        # set key-table mode-in
EOF
}

# ARGS: #{q:@mode}: #{window_id}: #{q:@mode-window}: #{q:@mode-window-last}: #{W:#{window_id}:}
# ARGS: [return]
func[mode-out]=1
mode-out(){
    local mode= flag= curwin= modewin= modewinlast= wins= selected=
    { read mode; read flag curwin modewin modewinlast wins; } <<<$(tmux source - <<'EOF'
        display -p "#{q:@mode}:"
        set @mode 0
        set key-table root
        set -F status-style "#{@status-out-style}"
        # set -u status-style
        #set -g status on
        display -p "! #{window_id}: #{q:@mode-window}: #{q:@mode-window-last}: #{W:#{window_id}:}"
EOF
        )
    [ "$flag" = '!' ] || return 1
    [ "$mode" != '1:' ] && return
    [ "$curwin" = ':' ] && return
    [ "$modewin" = ':' ] && return
    [[ "$wins" =~ "$modewinlast" ]] || return 0
    if [ "$1" = return ]; then
        if [ "$modewinlast" != ':' ] && [[ "$wins" =~ "$modewinlast" ]]; then
            tmux select-window -t "${modewinlast%:}" \; select-window -t "${modewin%:}"
        else
            tmux select-window -t "${modewin%:}"
        fi
    else
        tmux select-window -t "${modewin%:}" \; select-window -t "${curwin%:}"
    fi
}
func[rebound-mode-out]=1
rebound-mode-out(){
    mode-out "$@"
}

# ARG: [-n] [-c] [-p] [buffer]
func[copy]=1
copy(){
    local sel=1
    local prim=
    local clip=
    local buffer=()
    while [ $# -gt 0 ]; do
        case "$1" in
            -n) sel=             ;;
            -c) clip=1           ;;
            -p) prim=1           ;;
       -pc|-cp) clip=1;prim=1    ;;
             *) buffer=(-b "$1") ;;
        esac
        shift
    done
    [ ! $prim ] && [ ! $clip ] && clip=1 && prim=1
    [ $sel ] && tmux send -X copy-selection-no-clear "${buffer[@]}"
    if [ "$(command -v xclip)" ]; then
        [ $clip ] && tmux save-buffer "${buffer[@]}" - | xclip -i -sel clip
        [ $prim ] && tmux save-buffer "${buffer[@]}" - | xclip -i -sel prim
    elif [ "$(command -v xsel)" ]; then
        [ $clip ] && tmux save-buffer "${buffer[@]}" - | xsel -i -b
        [ $prim ] && tmux save-buffer "${buffer[@]}" - | xsel -i -p
    else
        echo 'no xclip & no xsel' >&2
    fi
}

# ARG: [-n] [-c] [-p] [buffer]
func[paste]=1
paste(){
    local paste=1
    local src=clip
    local buffer=()
    while [ $# -gt 0 ]; do
        case "$1" in
            -n) paste=           ;;
            -c) src=clip         ;;
            -p) src=prim         ;;
             *) buffer=(-b "$1") ;;
        esac
        shift
    done
    if [ "$(command -v xclip)" ]; then
        xclip -o -sel $src | tmux load-buffer "${buffer[@]}" - || true
    elif [ "$(command -v xsel)" ]; then
        [ $src = clip ] && s=b || s=p
        xsel -o -$s | tmux load-buffer "${buffer[@]}" - || true
    else
        echo 'no xclip & no xsel' >&2
    fi
    [ $paste ] && tmux paste-buffer -p "${buffer[@]}"
}

# tmux save-buffer - | xsel -i -p && xsel -o -p | xsel -i -b  # ???
# xsel -o | tmux load-buffer -                                # ???

# ARG: #{q:client_tty}
func[beep]=1
beep(){
    (   printf \\a
        sleep 0.1
        printf \\a
    ) > "$1" &
}

# ARG: #{q:selection_present}
func[copy-mode-escape]=1
copy-mode-escape(){
    if [ "$1" = 1 ]; then
        tmux send-keys -X clear-selection
    else
        tmux send-keys -X cancel
    fi
}

# ARG: #{q:window_width}
func[interactive-resize-window]=1
interactive-resize-window() {
    local cw ww nw w
    if [ "$1" == "-" ]; then
        cw=$2
        ww=$3
        nw=$4
        [ "$nw" != "$ww" ] || return 0
        nw=${nw//[^0-9]/}
        [ "$nw" ] || nw=0
        tmux set -uw window-size 
        if [ "$nw" -ne 0 ]; then
            if [ "$nw" -gt 10000 ]; then
                tmux resize-window -x 10000
            else
                tmux resize-window -x "$nw"
            fi
        fi
        w=$(tmux display -p '#{window_width}')
    else
        w="$1"
    fi
    tmux command-prompt -p 'WIDTH:' -I "$w" 'set -w @_ "%%%" ; run "#{E:@tmux}/sh interactive-resize-window - #{client_width} #{window_width} #{q:@_}"'
    return 0
}

# ARG: prev|next #{q:window_id}
func[swap-window]=1
swap-window() {
    if [ "$1" = prev ]; then
        tmux swap-window -d -s "$2" -t -1
    else
        tmux swap-window -d -s "$2" -t +1
    fi
}

func[cleanup]=1
cleanup() {
    "$tmux/tm" cleanup
}

########################################
# Hooks
########################################

func[client-session-changed]=1
client-session-changed() {
    cleanup
}


func[client-detached]=1
client-detached() {
    cleanup
}

# ARG: #{q:session_id} #{q:session_many_attached}
func[client-detached-modal]=1
client-detached-modal() {
    [ "$1" ] && [ "$2" = 0 ] && mode-out
    client-detached
}

# ARG: #{q:@mode-rebind}
func[after-bind-key-modal]=1
after-bind-key-modal() {
    mode-rebind "$1"
}

# # ARG: #{q:@mode-rebind}
# func[after-unbind-key-modal]=1
# after-unbind-key-modal() {
#     mode-rebind "$1"
# }

func[pane-mode-changed-modal]=1
pane-mode-changed-modal() {
    mode-out
}

b64() {
    if [ $# -gt 0 ]; then
        echo '"$(echo '$(IFS=$'\n'; echo -n "$*" |base64)' |base64 -d)"'
    else
        echo '"$(echo '$(base64)' |base64 -d)"'
    fi
}


# ARG: .#{pane_id} .#{@rlwrapper} .#{@rlwrappee}
func[rlwrap]=1
rlwrap(){
    #set -x
    local pane_id=${1#.} rlwrapper=${2#.} rlwrappee=${3#.}
    if [ "$rlwrappee" ]; then
        tmux select-pane -t "$rlwrappee" \
        || tmux kill-pane
        return
    fi
        #rlwrap -m -r -f . -c -C tmux-rlwrap -S "'$'\e[1m'': " \
    [ "$rlwrapper" ] && tmux select-pane -t "$rlwrapper" && return || true
    local tmp=$(mktemp) atexit=
    atexit -v atexit rm -f "$tmp"
    local _tmp=$(b64 "$tmp")

    tmux capture-pane -S - -E - -pJ \
      \; split -l 2 '
        touch ~/.tmux-rlwrap_history
        chmod go-rwx ~/.tmux-rlwrap_history
        mkdir -p ~/.rlwrap \
        && cd ~/.rlwrap \
        && exec rlwrap -C tmux-rlwrap -S "'$'\e[1m'': " \
                       -m -r -f '"$_tmp"' -f . -c -s 99999 \
            bash -c '\''
                rm -f '"$_tmp"'
                pane_id='"$(b64 "$pane_id")"'
                tmux set -p @rlwrappee "$pane_id" \
                  \; set -p -t {last} -F @rlwrapper "#{pane_id}" \
                  || exit 1
                while read -r l; do
                    if [ "$l" ]; then
                        echo "$l" \
                        | tmux load-buffer /dev/stdin \
                            \; paste-buffer -p -t "$pane_id" \
                        || break
                    else
                        tmux pipe-pane -t "$pane_id" -I echo
                    fi
                done
            '\'' \
        ' \
        >"$tmp"
    notatexit $atexit
}

# ARG: .#{q:window_name} .#{q:default-shell} .#{q:default-command}
func[new-window]=1
new-window() {
    local window=${1:1} shell=${2:1} command=${3:1}
    if ! [ "$window" ] || [ "$window" = "${shell##*/}" ] || [ "$window" = "${command##*/}" ]; then
        tmux set -w @random_name $(perl -e '$o=ord("A");while($i++<5){print chr($o+rand(26))}')
    fi
}

func[fixnames]=1
fixnames() {
    tmux display -p $'#{q:default-shell}\n#{q:default-command}\n' \
        \; list-windows -a -F '#{window_id} #{window_name}' \
        | perl -e '
            $A = ord("A");
            %shell = ("", 1);
            $i = 0;
            while (<>) {
                chomp;
                if ($. < 3) {
                    s@^.*/(.*)$@$1@;
                    $shell{$_} = 1;
                } else {
                    if (/^(\S+)\s(.*)/ && $shell{$2}) {
                        $i=0;
                        $r="";
                        while ($i++<3) {$r.=chr($A+rand(26))}
                        printf("rename-window -t %s %s\n", $1, $r);
                    }
                }
            }
        ' \
        | tee /dev/stderr \
        | tmux source -
}

# ARGUMENTS[fakezoom]= (does not expand @fakezoom for the right window/moment)
# ARG: {toggle [width [height]]
func[fakezoom]=1
fakezoom(){
    #set -x
    local cmd=$1 width=${2:-100%} height=${3:-100%}
    local fakezoom _fakezoom zoom pane _pane _width _height __width __height
    exec 3< <(tmux \
        display -p '.#{pane_id} .#{window_zoomed_flag} .#{q:@fakezoom}' \
        \; list-panes -F '.#{pane_id} .#{pane_width} .#{pane_height} .#{q:@fakezoom-width} .#{q:@fakezoom-height}' \
    )
        #| tee -a /dev/stderr \
    read -u 3 pane zoom fakezoom
    pane=${pane:1} zoom=${zoom:1} fakezoom=${fakezoom:1}
    if [ "$cmd" = 'refresh' ] || [ "$cmd" = 'layout' ]; then
        [ "$fakezoom" ] || return 0
        [ "$cmd" = 'layout' ] || fakezoom=
    fi
    if [ "$zoom" = 1 ]; then
        zoom=$(tmux resize-pane -Z \; refresh-client \; display -p '#{window_zoomed_flag}')
        if [ "$zoom" = 1 ]; then
            echo '::: error: cannot unzoom' >&2
            return 1
        else
            fakezoom "$cmd" "$width" "$height"
            return $?
        fi
    fi
    {
        #[ "$zoom" != 1 ] || echo "resize-pane -Z"
        [ "$cmd" = 'refresh' ] || [ "$fakezoom" ] || echo "set -w @fakezoom 1"
        while read _pane _width _height __width __height; do
            _pane=${_pane:1} _width=${_width:1} _height=${_height:1} __width=${__width:1} __height=${__height:1}
            #echo "? _pane=${_pane@Q} _width=${_width@Q} _height=${_height@Q} __width=${__width@Q} __height=${__height@Q}" >&2
            if [ "$fakezoom" ]; then
                [ "$__width" ] && [ "$__height" ] \
                    && echo "resize-pane -t $_pane -x $__width -y $__height"
                echo "set-option -t $_pane -p @fakezoom-width ''"
                echo "set-option -t $_pane -p @fakezoom-height ''"
            else
                echo "resize-pane -t $_pane -x $width -y $height"
                if [ "$cmd" != refresh ]; then
                    echo "set-option -t $_pane -p @fakezoom-width $_width"
                    echo "set-option -t $_pane -p @fakezoom-height $_height"
                fi
                [ "$_pane" != "$pane" ] || break
            fi
        done
        [ "$cmd" != 'refresh' ] && [ "$fakezoom" ] && echo "set -w @fakezoom ''"
    } <&3 \
    | tmux source -
    #tmux list-panes -F 'kill -WINCH #{pane_pid}' | tee -a /dev/stderr | sh
    #| tee -a /dev/stderr \
}

func[nview]=1
nview() {
    echo not implemented
    return 1
    local width="$1" height="$2" S="${3:--}" scrollback="${4:-9999}"
    ([ "$((0+scrollback))" ]) || return 1
        #\; split-window "nvim -c 'set scrollback=$scrollback' -c 'term tmux show-buffer;read' -c 'exe \"normal G2\\<c-y>\"'" \
    tmux capture-pane -S "$S" -E - -eJ \
        \; new-window -n pager 'nvim -c "set scrollback=99999 laststatus=0 cmdheight=1" -c "term tmux show-buffer|;read" -c "stopinsert" -c "normal G"'
}

# # ARGUMENTS[select-layout]='--'\
# # ' .#{window_width}'\
# # ' .#{window_height}'\
# # ' .#{q:@main-other-pane-width-height-percent}'\
# # ' --' # SELECT-LAYOUT_ARGS...
# select-layout(){
#     local width=${2:1} height=${3:1} setup=${4:1}; shift 5
#     local ops=
#     if [ "$setup" ]; then
#         local mx my ox oy
#         IFS=, read -r mx my ox oy < <(echo -n "$setup")
#         ! [ "$mx" ] || ops="set -w main-pane-width   $((mx*100/width))  \\; $ops"
#         ! [ "$my" ] || ops="set -w main-pane-height  $((my*100/height)) \\; $ops"
#         ! [ "$ox" ] || ops="set -w other-pane-width  $((ox*100/width))  \\; $ops"
#         ! [ "$oy" ] || ops="set -w other-pane-height $((oy*100/height)) \\; $ops"
#     fi
#     tmux $ops set -w @fakezoom '' \; select-layout "$@"
# }

#$(
#    (
#        exec >"$fifo"
#        timeout 5 echo -n "" || exit 1
#        rm -rf "$tmp"
#        tmux capture -p
#    ) </dev/null >/dev/null &
#    echo "$tmp/f"
#)

########################################
# misc
########################################

# func[maybe-resize]=1
# maybe-resize() {
#     mode=$1 # me / me-if-recent / recent-not-me
#     force=$2
#     {
#         read -r WI CH CW WH WW ST NO CN
#         CH=$((10#$CH))
#         CW=$((10#$CW))
#         if [ ! "$mode" = recent-not-me ]; then
#             [ "$force" ] || [ "$NO" = X ] || return
#             [ ! "$CH" = 0 ] && [ ! "$CW" = 0 ] || return
#         fi
#         if [ ! "$mode" = me ]; then
#             FI=0
#             _CN=$CN # just in case...
#             while read -r wi ch cw fi st no cn; do
#                 fi=$((10#$fi))
#                 ch=$((10#$ch))
#                 cw=$((10#$cw))
#                 [ "$fi" -gt "$FI" ] || continue
#                 [ "$wi" = "$WI" ] || continue
#                 [ "$force" ] || [ "$no" = X ] || continue
#                 [ "$mode" = recent-not-me ] && [ "$cn" = "$CN" ] && continue
#                 [ ! "$ch" = 0 ] && [ ! "$cw" = 0 ] || continue
#                 CH=$ch
#                 CW=$cw
#                 FI=$fi
#                 ST=$st
#                 _CN=$cn
#             done
#             if [ "$mode" = me-if-recent ]; then
#                 [ "$_CN" = "$CN" ] || return
#             else
#                 [ "$FI" = 0 ] && return
#             fi
#         fi
#         [ "$ST" = Xon ] && CH=$((CH-1))
#         [ "$WH" = "$CH" ] && [ "$WW" = "$CW" ] && return
#         tmux resize-window -y "$CH" -x "$CW"
#     } <<<$(tmux \
#         display -p '#{window_id} 0#{client_height} 0#{client_width} #{window_height} #{window_width} X#{status} X#{noresize} #{client_name}' \;\
#         list-clients -F '#{window_id} 0#{client_height} 0#{client_width} 0#{focusidx} X#{status} X#{noresize} #{client_name}' \
#     )
# }


########################################

# func[pane-focus-in]=1
# pane-focus-in() {
#     tmux set-option @focusidx $idx
#     return # XXX
#     maybe-resize me
# }

########################################

# func[pane-focus-out]=1
# pane-focus-out() {
#     return # XXX
#     maybe-resize recent-not-me
# }

########################################

# func[client-resized]=1
# client-resized() {
#     return # XXX
#     maybe-resize me-if-recent
# }

########################################

# func[client-detached]=1
# client-detached() {
#     tm --cleanup
# }

########################################

# func[init]=1
# init(){
#     # set-hook -gu pane-focus-in
#     # set-hook -g pane-focus-in {run-shell "#{E:@func} pane-focus-in"}
# 
#     # set-hook -gu pane-focus-out
#     # set-hook -g pane-focus-out {run-shell "#{E:@func} pane-focus-out"}
# 
#     # set-hook -gu client-resized
#     # set-hook -g client-resized {run-shell "#{E:@func} client-resized"}
# 
#     # tmux set-hook -gu client-session-changed \; \
#     #      set-hook -g client-session-changed 'run-shell "#{E:@func} client-session-changed"' \; \
#     #      set-hook -gu client-detached \; \
#     #      set-hook -g client-detached 'run-shell "#{E:@func} client-detached"'
# }

# func[init]=1
# init(){
#     return
# #     # stage=$1
# #     # initialized=$2
# # 
# #     # if [ -n "$initialized" ]; then
# #     #     if [ "$stage" = "pre" ]; then
# #     #         exit 0
# #     #     else
# #     #         exit 0
# #     #     fi
# #     # else
# #     #     if [ "$stage" = "pre" ]; then
# #     #         exit 0
# #     #     else
# #     #         exit 0
# #     #         # "$(dirname "$0")/all-hooks.sh" -ga 'set -gF @_my_last "#{session_id}:#{window_index}.#{pane_index}"'
# #     #     fi
# #     # fi
# }

########################################

# func[xinit]=1
# xinit(){
#     return
#     # cmd=
#     # tmux list-windows -aF '#{window_id}' \
#     # | (
#     #     unset wins
#     #     declare -A wins
#     #     while read w; do
#     #         [ "${wins[$w]}" ] && continue
#     #         wins[$w]=1
#     #         cmd="$cmd set -wt $w window-size smallest ;"
#     #     done
#     #     [ "$cmd" ] && tmux $cmd || true
#     # )
# }

########################################
# Main
########################################

PS4='+ #${LINENO} '
# set -x

[ $# -ne 0 ] || set -- init
f=$1
shift

if [ "${func[$f]}" ]; then
    "$f" "$@" || echo "command failed: $f" >&2 || true
else
    echo "command invalid: $f" >&2 || true
fi
