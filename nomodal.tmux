set -g @modal 0
unbind -n `
set -g prefix `
bind ` send-prefix
bind -T prefix -r Enter  set key-table root
bind -T prefix -r Escape set key-table root
bind -T prefix -r C-c    set key-table root

set-hook -ga client-session-changed 'run "#{q:@tmux}/sh client-session-changed"'
set-hook -ga client-detached        'run "#{q:@tmux}/sh client-detached"'

########################################

# # My mode (incomplete)

# unbind -n M-`
# set -g prefix M-`
# bind 'M-`' send-prefix
# 
# bind -r Escape set key-table root
# # bind -r Enter  set key-table root
# # bind Enter  set key-table root
# bind Enter  start
