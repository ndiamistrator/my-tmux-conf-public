#set -g status-position top

# set -g @status-mode "#{@mode}"
set -g @status-mode "#{!=:#{client_key_table},root}"

set -g @status-out-style "reverse fg=blue bg=brightwhite"
##set -g @status-reverse-out-style noreverse

set -g @status-in-style  "reverse fg=brightgreen bg=black"
##set -g @status-reverse-in-style  noreverse
# set -F -g @status-reverse-in-style  "#{@status-reverse-out-style}"

set -g -F status-style          "#{@status-out-style}"
set -g -F message-style         "#{@status-in-style}"

set -g -F message-command-style "#{@status-out-style}"
# set -g message-command-style    'fg=brightgreen bg=blue reverse'
set -g mode-style               'fg=brightgreen bg=blue reverse'

set -g pane-border-style        'bg=blue fg=blue'
set -g pane-active-border-style 'bg=green fg=green'

set -g display-panes-colour        'brightgreen'
set -g display-panes-active-colour 'brightblue'

set -g @status-outstand-style "#[fg=black bg=white bold]"
set -g @status-outstand2-style "#[fg=brightwhite bg=black bold]"

set -g status on
set -g status-keys vi

set -g status-interval 1
set -g @blink '#{==:#{m/r:[02468]$,%s},#{m/r:[02468]$,#{client_activity}}}'

##set -g @status-reverse-style "#[#{?#{E:@status-mode},#{@status-reverse-in-style},#{@status-reverse-out-style}}]"
set -g @status-blink-style "#{?#{T:@blink},#{T:@status-outstand2-style},#{T:@status-outstand-style}}"
##set -g @status-phased-blink-style "#{?#{T:@blink},#{T:@status-outstand-style},#{T:@status-outstand2-style}}#[blink]"
set -g @status-session-style "#{?#{E:@status-mode},#[bg=black fg=brighgreen],#[bg=brightyellow fg=blue]}#[bold]"
set -g @status-pane-style ""
set -g @status-mode-style "#{?#{T:@blink},#{T:@status-outstand-style},#{T:@status-outstand2-style}}"
set -g @status-prefix-style "#{E:@status-mode-style}"
# set -g @status-separator-style '#[default]'

set -g @window-status-style         "#{?#{E:@status-mode},#[bg=black fg=brighgreen],#[bg=brightwhite fg=blue]}"
set -g @window-status-current-style "#{?#{E:@status-mode},#[bg=brightgreen fg=blue bold],#[bg=blue fg=brightgreen bold]}"
set -g @window-status-last-style    "#{?#{E:@status-mode},#[bg=blue fg=brightgreen bold],#[bg=brightgreen fg=blue bold]}"
# set -g @window-status-current-style "#{?#{E:@status-mode},#{E:@status-blink-style},#{E:@status-reverse-style}}#[bold]"
set -g @window-status-marked-style "#[strikethrough bold]"
set -g @window-status-alert-style "#{?#{T:@blink},#[noreverse],}#[bold]"
#set -g @window-status-zoomed-style "#{E:@status-outstand-style}"
set -g @window-status-zoomed-style "#{?#{T:@blink},#{T:@status-outstand2-style},#{T:@status-outstand-style}}"
set -g @window-status-sized-style "#{?#{T:@blink},#{T:@status-outstand2-style},#{T:@status-outstand-style}}"

set -g @status-id-style "#[bg=black]"

set -g status-justify left
set -g status-left-length 0
set -g status-right-length 0

set -g @status-head "#{@status-socket}#{?#{!=:#{session_group},},-t #{session_group},-n #{session_name}}"
set -g @status-window-name "#{?#{||:#{==:#{window_name},},#{||:#{==:#{window_name},#{b:default-shell}},#{==:#{window_name},#{b:default-command}}}},, #{window_name}}"

set -g status-left '#{E:@status-session-style} #{host_short} #{E:@status-head} #[default]#[pop-default] '
set -g status-right '#{?#{!=:#{window-size},latest}, #[push-default]#{E:@window-status-sized-style} SIZED #[default]#[pop-default],}#{?window_zoomed_flag, #[push-default]#{E:@window-status-zoomed-style} ZOOM #[default]#[pop-default],}#{?#{||:#{!=:#{client_key_table},root},#{client_prefix}}, #[push-default]#{E:@status-prefix-style} #{client_key_table} #[default]#[pop-default],}#{?pane_in_mode, #[push-default]#{E:@status-mode-style} #{pane_mode} #[default]#[pop-default],} #{E:@status-id-style}#{session_id}:#{window_id}:#{pane_id}'

set -g window-status-separator ''
set -g @window-status-format " #[push-default]#{?#{||:#{window_activity_flag},#{||:#{window_bell_flag},#{window_silence_flag}}},#{E:@window-status-alert-style},}#{?window_marked_flag,#{E:@window-status-marked-style},}(#{window_index})#{E:@status-window-name}#[default]#[pop-default] "
set -g window-status-format "#{?window_last_flag,#{E:@window-status-last-style},#{E:@window-status-style}}#{E:@window-status-format}"
set -g window-status-current-format "#{E:@window-status-current-style}#{E:@window-status-format}"

# better define style in corresponding format, because no* instructions do not work in *-style
set -g window-status-current-style ''
set -g window-status-style ''
set -g window-status-last-style ''

set -g window-status-activity-style ''
set -g window-status-bell-style ''

########################################

# ORIGINAL status-format[0]:
# set -g status-format[0] "#[align=left range=left #{status-left-style}]#[push-default]#{T;=/#{status-left-length}:status-left}#[pop-default]#[norange default]#[list=on align=#{status-justify}]#[list=left-marker]<#[list=right-marker]>#[list=on]#{W:#[range=window|#{window_index} #{window-status-style}#{?#{&&:#{window_last_flag},#{!=:#{window-status-last-style},default}}, #{window-status-last-style},}#{?#{&&:#{window_bell_flag},#{!=:#{window-status-bell-style},default}}, #{window-status-bell-style},#{?#{&&:#{||:#{window_activity_flag},#{window_silence_flag}},#{!=:#{window-status-activity-style},default}}, #{window-status-activity-style},}}]#[push-default]#{T:window-status-format}#[pop-default]#[norange default]#{?window_end_flag,,#{window-status-separator}},#[range=window|#{window_index} list=focus #{?#{!=:#{window-status-current-style},default},#{window-status-current-style},#{window-status-style}}#{?#{&&:#{window_last_flag},#{!=:#{window-status-last-style},default}}, #{window-status-last-style},}#{?#{&&:#{window_bell_flag},#{!=:#{window-status-bell-style},default}}, #{window-status-bell-style},#{?#{&&:#{||:#{window_activity_flag},#{window_silence_flag}},#{!=:#{window-status underscore-activity-style},default}}, #{window-status-activity-style},}}]#[push-default]#{T:window-status-current-format}#[pop-default]#[norange list=on default]#{?window_end_flag,,#{window-status-separator}}}#[nolist align=right range=right #{status-right-style}]#[push-default]#{T;=/#{status-right-length}:status-right}#[pop-default]#[norange default]"
