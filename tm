#!/bin/sh
#set -x
set -e
umask 077

#echo "make me" >&2
#exit 1


TIMEOUT=2

#timeout=timeout
tmux=tmux

define_sockets() {
    SOCKETS="/tmp/tmux-$(id -u)"
}

########################################

ARGS="$*"
# gets updated bellow with quot

usage() {
    define_sockets
    program="$(basename "$0")"
    cat <<EOF >&2
\$ $program $ARGS

Usage:

Session:
    Start a new tmux session, grouped with another:
      $program [-|-m|-t TARGET] [-i] [--debug]
          [TMUX-GEN-OPT...] [TMUX-SES-OPT...] [TMUX-CMD...]

    TMUX-CMD...  without  [-|-m|-t TARGET]  does NOT start a new session!

    Clean up abandoned tmux sessions:
      $program [--debug] [TMUX-GEN-OPT...] cleanup

    Options:
            -) Group with last active pane (default if no TMUX-CMD...)
           -m) Group with marked pane
           -t) Group with TARGET session
      --debug) Verbose dry run
           -i) Start inside custom NVIM terminal (TODO)

    TMUX options supported (v3.1c):
        TMUX-GEN-OPT
            -[uv]
           -[fLS] ARG
        TMUX-SES-OPT
           -[2Cl]
             -[c] ARG

Sockets:
    Clean up abandoned tmux sockets:
      $program [--debug] cls [-d SOCKETS-DIR] [-t TIMEOUT]

    List active tmux sockets:
      $program [--debug] ls  [-d SOCKETS-DIR] [-t TIMEOUT]

    Default:
        SOCKETS-DIR: $SOCKETS
            TIMEOUT: $TIMEOUT
 
EOF
    [ $# = 0 ] || printf 'ERROR: %s\n' "$*" >&2
    exit 1
}

########################################

# git@gitlab.com:ndiamistrator/my-shell.git
# aea737391df84767d31a0633682383facd7ae76d/quot.sh

shreplac() {
    local _string=$1 _pattern=$2 _prefix=$3 _suffix=$4 # _replacement=$5
    local _shrepl=
    local _tmp _left
    while [ "$_string" ]; do
        _tmp=${_string#*$_pattern}
        _tmp=${_string%"$_tmp"}
        [ "$_tmp" ] || ! _shrepl="$_shrepl$_string" || break
        _left=${_tmp%$_pattern}
        [ $# -gt 4 ] \
        && _shrepl="$_shrepl$_left$_prefix${5}$_suffix" \
        || _shrepl="$_shrepl$_left$_prefix${_tmp#"$_left"}$_suffix"
        _string=${_string#"$_tmp"}
    done
    __=$_shrepl
}

shquot() {
    [ $# -eq 0 ] && __= && return
    shreplac "$1" "'" '' '' "'\"'\"'"
    local _shquot="'$__'"
    [ $# -eq 1 ] && __=$_shquot && return
    shift
    local _word
    for _word in "$@"; do
        shreplac "$_word" "'" '' '' "'\"'\"'"
        _shquot="$_shquot '$__'"
    done
    __=$_shquot
}

quot() {
    shquot "$@"
}

########################################

#_timeout() {
#    local timeout="$1"
#    shift
#    set -m
#    timeout() {
#        sleep "$timeout"
#        printf '::: _timeout expired!\n' >&2
#    }
#    job() {
#        "$@"
#    }
#    cleanup() {
#    }
#    atexit() {
#        local _s="$1"
#        trap - EXIT
#        trap - CHLD
#        printf '::: _timeout atexit: %s\n' "$_s" >&2
#        jobs %job >/dev/null 2>&1 && _s=124 || [ "$_s" -eq 0 ] || _s=1
#        ! kill %job 2>/dev/null
#        ! kill %timeout 2>/dev/null
#        wait
#        printf '::: _timeout bye!\n' >&2
#        exit $_s
#    }
#    child() {
#        trap - CHLD  # XXX
#        printf '::: _timeout SIGCHLD\n' >&2
#        jobs -l >&2
#        exit 0
#    }
#    trap child CHLD
#    trap atexit EXIT
#    printf '::: _timeout hello!\n' >&2
#    job "$@" &
#    timeout &
#    wait
#    printf '::: _timeout done waiting!\n' >&2
#}
#
#set -x
#_timeout $TIMEOUT sleep ${1:-3}
#exit

################################################################################
# main
################################################################################

#if [ "$1" = --- ]; then
#    shift
#    "$@"
#    exit
#fi

########################################

_arg() {
    eval "
        quot \"\$2\"
        $1=\"\$$1 \$__\"
    "
}

group=
nvim=
debug=

genopt=
sesopt=

########################################

EXEC=exec

_tmux() {
    if [ "$1" = - ]; then
        shift
        if [ "$debug" ]; then
            eval "quot \"\$tmux\" $genopt $sesopt \"\$@\""
            printf '::: %s\n' "$__" >&2
            exit 0
        else
            eval "\$EXEC \"\$tmux\" $genopt $sesopt \"\$@\""
            exit $?
        fi
    else
        eval "command \"\$tmux\" $genopt \"\$@\""
    fi
}

########################################

quot "$@"
ARGS="$__"

while [ $# -gt 0 ]; do
    case "$1" in 
        -[cfLS]?*) # unmerge 2nd arg
            _suffix=${1#??}
            _prefix=${1%"$_suffix"}
            shift
            set -- "$_prefix" "$_suffix" "$@"
            continue
            ;;
        -[2CluvV]?*|-[mtni]?*) # unmerge options
            _suffix=${1#??}
            _prefix=${1%"$_suffix"}
            shift
            set -- "$_prefix" -"$_suffix" "$@"
            continue
            ;;
        -[uv]) # single TMUX-GEN-OPT
            _arg genopt "$1"
            ;;
        -[2Cl]) # single TMUX-SES-OPT
            _arg sesopt "$1"
            ;;
        -[fLS]) # paired TMUX-GEN-OPT
            [ $# -gt 1 ] || usage "missing argument: $1"
            _arg genopt "$1"
            _arg genopt "$2"
            shift
            ;;
        -[c]) # paired TMUX-SES-OPT
            [ $# -gt 1 ] || usage "missing argument: $1"
            _arg sesopt "$1"
            _arg sesopt "$2"
            shift
            ;;
        -) # Group with last active pane
            group=-
            ;;
        -m) # Group with marked pane
            group={marked}
            ;;
        -t) # Group with TARGET
            [ $# -gt 1 ] || usage "missing argument: $1"
            group=$2
            shift
            ;;
        -i) # Start inside custom NVIM terminal
            usage 'not implemented: -i'
            #nvim=1
            ;;
        --debug) # Verbose dry run
            debug=1
            ;;
        -h|--help)
            usage
            ;;
        -*)
            usage "invalid argument: $1"
            ;;
        *)
            break
            ;;
    esac
    shift
done

#[ "$debug" ] && set -x

################################################################################
# subcommand
################################################################################

case "$1" in

cleanup)
    shift
    if [ "$group" ] || [ $# -gt 0 ] || [ "$sesopt" ]; then
        usage 'cleanup: ambiguous arguments'
    fi
    (_tmux list-sessions -F ':#{session_attached} :#{session_id} #{session_group}' \
     || printf '\nerror\n') | (
        #set -x
        set --
        split=0
        while read -r a s g; do
            [ "$a" != error ] || exit 1
            [ "$g" ] || continue
            if [ "$a" = :1 ]; then
                set -- "$@" "$g"
            else
                set -- "${s#:}" "$g" "$@"
                split=$((split+2))
            fi
        done
        check() {
            local x v="$1"
            shift $((split+1))
            for x in "$@"; do
                [ "$x" != "$v" ] || return 1
            done
        }
        kill=
        while [ "$split" -gt 0 ]; do
            s="$1"
            g="$2"
            shift 2
            split=$((split-2))
            check "$g" "$@" \
            && set -- "$@" "$g" \
            || kill="kill-session -t '$s' ';' $kill"
        done
        [ "$kill" ] || exit 0
        eval "_tmux - $kill"
    )
    ;;

cls|ls)
    #verbose=
    #sep=' ' end='\n'
    ls_cut='3-'
    cmd=$1
    shift
    ! [ "$group$sesopt" ] || usage "$cmd: ambiguous arguments"
    define_sockets
    while [ $# -gt 0 ]; do
        case "$1" in
        -d|-t)
            [ $# -gt 1 ] || missing "$1"
            [ "$1" = '-d' ] && SOCKETS="$2" || TIMEOUT="$2"
            shift
            ;;
        -f)
            ls_cut='1-'
            ;;
        #-s)
        #    [ "$cmd" = ls ] || usage "invalid argument: $1"
        #    [ $# -gt 1 ] || missing "$1"
        #    sep="$2"
        #    shift
        #    ;;
        #-e)
        #    [ "$cmd" = ls ] || usage "invalid argument: $1"
        #    [ $# -gt 1 ] || missing "$1"
        #    end="$2"
        #    shift
        #    ;;
        #-v)
        #    [ "$cmd" = ls ] || usage "invalid argument: $1"
        #    verbose=1
        #    ;;
        -)
            usage "invalid argument: $1"
            ;;
        *)
            SOCKETS="$1"
            ;;
        esac
        shift
    done

    #if [ "$TIMEOUT" ] && ! [ "$TIMEOUT" = 0 ] && [ "$timeout" ]; then
    #    if ! command -v "$timeout" >/dev/null; then
    #        printf 'command not found: %s\n' "$timeout" >&2
    #        return 1
    #    fi
    #    _timeout() {
    #        "$timeout" "$TIMEOUT" "$@"
    #    }
    #else
    #    _timeout() {
    #        "$@"
    #    }
    #fi
    #timeout() {
    #    printf '::: _timeout %s\n' "$*" >&2
    #    if [ "$TIMEOUT" ] && ! [ "$TIMEOUT" = 0 ] && [ "$timeout" ]; then
    #        #if ! command -v "$timeout" >/dev/null; then
    #        #    printf 'command not found: %s\n' "$timeout" >&2
    #        #    return 1
    #        #fi
    #    else
    #        "$@"
    #    fi
    #}

    _timeout_init=
    timeout_init() {
        [ "$_timeout_init" ] && return "$_timeout_init"
        command timeout 1 true && _timeout_init=0 || _timeout_init=1
        return "$_timeout_init"
    }

    timeout() {
        if [ "$1" ] && ! [ "$1" = 0 ] && timeout_init; then
            #/bin/sh "$0" --- _timeout "$1" "$@"
            command timeout "$@"
        else
            shift
            command "$@"
        fi
    }

    cleanup_sockets() {
        if ! command -v "$tmux" >/dev/null; then
            printf 'command not found: %s\n' "$tmux" >&2
            return 1
        fi
        local _sock _name _date=
        for _sock in "$SOCKETS"/*; do
            [ -S "$_sock" ] || continue
            if timeout "$TIMEOUT" "$tmux" -f /dev/null -S "$_sock" set -gu @_ 2>/dev/null; then
                continue
            elif [ $? = 124 ]; then
                printf 'timeout: %s\n' "$_sock" >&2 || true
                continue
            fi
            if [ "$debug" ]; then
                #set +x
                quot "$_sock"
                #set -x
                printf '::: rm %s\n' "$__" >&2
                continue
            fi
            if ! [ "$_date" ]; then
                mkdir -p "$SOCKETS/.cleanup"
                _date=$(date +%s)
            fi
            _name=${_sock#"$SOCKETS/"}
            rm -f "$SOCKETS/.cleanup/$_name.$_date"
            mv -f "$_sock" "$SOCKETS/.cleanup/$_name.$_date"
        done
    }

    list_sockets() {
        #pipe() { awk -vPREFIX="$1" 'BEGIN {RS="'"${2:-\\n}"'"; ORS=RS} {print; printf("%s%s\n", PREFIX, $0) >>"/dev/stderr"}'; }
        local _sock
        #local         _fmt="%s$sep%s$sep%s$sep%3s$sep%4s$sep%1s$sep%1s$sep%15s$sep%15s$sep%15s$sep%s$end"
        #local _fmt_verbose="%s$sep%s$sep%s$sep%2s$sep%3s$sep%4s$sep%1s$sep%1s$sep%15s$sep%15s$sep%15s$sep%s$sep%s$sep%s$end"
        for _sock in "$SOCKETS"/*; do
            [ -S "$_sock" ] || continue
            timeout "$TIMEOUT" "$tmux" -f /dev/null -S "$_sock" display -p '#{S:#{W:
                printf "#{a:37}s\\0" \
                    socket_path=#{q:socket_path} \
                    _session=#{?#{!=:#{session_group},},-t#{session_group},-n#{session_name}} \
                    window_id=#{q:window_id} \
                    window_name=#{q:window_name} \
                    window_active_clients=#{q:window_active_clients} \
                    window_panes=#{q:window_panes} \
                    _activity_long=#{t/f/#{a:37}Y-#{a:37}m-#{a:37}d_#{a:37}H#:#{a:37}M#:#{a:37}S:window_activity} \
                    _activity_short=#{t/p:window_activity} \
                    ""}}' \
            || [ $? != 124 ] || printf 'timeout: %s\n' "$_sock" >&2
        done \
        | sh \
        | awk -vSOCKETS="$SOCKETS/" '
            BEGIN {
                RS="\0\0"
                FS="\0"
                SOCKETSLEN = length(SOCKETS)
            } {
                key = $2 ":" $4 ":" $3
                if (seen[key]) next
                seen[key] = 1
                nr++
                for (i=1;i<=NF;i++) {
                    key = $i
                    sub("=.*", "", key)
                    val = $i
                    sub("[^=]*=", "", val)
                    gsub("[^ -~]+", "?", val)
                    S[nr][key] = val
                }
                activity = S[nr]["_activity_long"]
                if (sockets_activity[S[nr]["socket_path"]] < activity)
                    sockets_activity[S[nr]["socket_path"]] = activity
                if (sessions_activity[S[nr]["_session"]]   < activity)
                    sessions_activity[S[nr]["_session"]]   = activity
            } END {
                for (n=1;n<=nr;n++) {
                    printf("%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\n",
                        sockets_activity[S[n]["socket_path"]],
                        sessions_activity[S[n]["_session"]],
                        S[n]["_activity_long"],

                        S[n]["_activity_short"],

                        "*" S[n]["window_active_clients"],
                        "x" S[n]["window_panes"],

                        (SOCKETS == substr(S[n]["socket_path"], 1, SOCKETSLEN) \
                        ? ("-L" substr(S[n]["socket_path"], SOCKETSLEN+1)) \
                        : ("-S" S[n]["socket_path"])),

                        S[n]["_session"],

                        S[n]["window_id"],
                        S[n]["window_name"])
                }
            }
        ' \
        | LC_ALL=C sort -k 3 -k 2 -k 1 \
        | awk 'BEGIN {FS="\t"} {if ($7!=__) {if (length(__)) {printf("\n")}; __=$7}; print}' \
        | cut -f "$ls_cut"
    }

    cleanup_sockets
    [ "$cmd" = ls ] && list_sockets "$@"
    ;;

*)
    if [ "$group" = {marked} ]; then

        _tmp=$(mktemp -d)
        [ "$_tmp" ] || exit 1
        mkfifo "$_tmp/fifo"
        exec 3<>"$_tmp/fifo"
        rm -rf "$_tmp"

        _tmux list-panes -a \
            -F ':#{window_marked_flag} :#{pane_marked} :#{session_id} :#{window_index} :#{pane_id}' \
            >&3

        _found=
        while read -r _window_marked_flag _pane_marked _session_id _window_index _pane_id; do
            if [ "$_window_marked_flag" = :1 ] && [ "$_pane_marked" = :1 ]; then
                _found=1
                break
            fi
        done <&3
        exec 3<&- 3>&-
        if [ "$_found" ]; then
            _tmux - new-session -t "${_session_id#:}" \
                \; select-window -t "${_window_index#:}" \
                \; select-pane -t "${_pane_id#:}" \
                \; "$@"
            exit $?
        else
            printf 'no marked pane\n' >&2
            exit 2
        fi

    elif  ! [ "$group" ] && [ $# -eq 0 ] || [ "$group" = - ]; then
        session="$(_tmux display -p '#{session_id}' 2>/dev/null || true)"
        if [ "$session" ]; then
            _tmux - new-session -t "$session" \; "$@"
        else
            _tmux - new-session \; "$@"
        fi
        #tmux list-session -F '#{window_activity} #{session_id} #{window_index}' \
        #| awk '$1 > t {t=$1; x=$2 " " $3} END {print x}'

    elif [ "$group" ]; then
        _tmux - new-session -t "$group" \; "$@"

    else
        _tmux - "$@"
    fi
    ;;
esac
