# Server

set -g exit-empty off
set -g exit-unattached off

# Limits

set -g history-limit 1000
set -g buffer-limit 1

# Terminal

set -g mouse on
set -g xterm-keys on
set -ag terminal-overrides 'xterm*:Tc'
set -ag terminal-overrides 'gnome*:Tc'
# set -g default-terminal tmux-256color
set -g default-terminal xterm-256color
set -g focus-events on
# set -g focus-events off
set -g set-titles on
set -g set-titles-string "#{E:@status-head}#{E:@status-window-name}#{?#T, | #T,}"

# Copy-mode

set -g mode-keys vi
set -g wrap-search off

# Windows

# set -g window-size smallest
# set -g aggressive-resize on
set -g window-size latest
set -g aggressive-resize off
set -g automatic-rename off
set -g renumber-windows on

# Status

set -g visual-bell off

# Times

set -g escape-time 20
set -g display-time 4000
set -g repeat-time 2000
# set -g repeat-time 5000

set -g main-pane-width 66%
set -g main-pane-height 66%
