# escape/cancel/clear
bind -T copy-mode-vi    Escape            run "#{q:@tmux}/sh copy-mode-escape #{q:selection_present}"
bind -T copy-mode       Escape            run "#{q:@tmux}/sh copy-mode-escape #{q:selection_present}"
bind -T copy-mode-vi    C-c               send-keys -X cancel
bind -T copy-mode       C-c               send-keys -X cancel

# xclip
bind                    ]                 run "#{q:@tmux}/sh paste"
#ind                    M-]               run "#{q:@tmux}/sh paste -p"

bind -T copy-mode-vi    Enter             {run "#{q:@tmux}/sh copy" ; send -X cancel}
bind -T copy-mode       Enter             {run "#{q:@tmux}/sh copy" ; send -X cancel}
bind -T copy-mode-vi    y                 {run "#{q:@tmux}/sh copy" ; send -X clear-selection}
bind -T copy-mode       y                 {run "#{q:@tmux}/sh copy" ; send -X clear-selection}
bind -T copy-mode-vi    MouseDragEnd1Pane send Enter
bind -T copy-mode       MouseDragEnd1Pane send Enter

# selection
bind -T copy-mode-vi    a                 send -X append-selection
bind -T copy-mode       a                 send -X append-selection
bind -T copy-mode-vi    v                 send -X begin-selection
bind -T copy-mode       v                 send -X begin-selection
bind -T copy-mode-vi    s                 send -X stop-selection
bind -T copy-mode       s                 send -X stop-selection
bind -T copy-mode-vi    C-v               send -X rectangle-toggle
bind -T copy-mode       C-v               send -X rectangle-toggle

# scroll in copy mode
bind -T copy-mode-vi    M-K               send -X scroll-up
bind -T copy-mode       M-K               send -X scroll-up
bind -T copy-mode-vi    M-k               send -X halfpage-up
bind -T copy-mode       M-k               send -X halfpage-up
bind -T copy-mode-vi    M-J               send -X scroll-down
bind -T copy-mode       M-J               send -X scroll-down
bind -T copy-mode-vi    M-j               send -X halfpage-down
bind -T copy-mode       M-j               send -X halfpage-down

# pane navigation
#no-r# bind -T prefix       -r h                 select-pane -L
bind -T prefix          h                 select-pane -L
#no-r# bind -T prefix       -r j                 select-pane -D
bind -T prefix          j                 select-pane -D
#no-r# bind -T prefix       -r k                 select-pane -U
bind -T prefix          k                 select-pane -U
#no-r# bind -T prefix       -r l                 select-pane -R
bind -T prefix          l                 select-pane -R

# pane resize
#no-r# bind -T prefix       -r M-h               resize-pane -L 5
bind -T prefix       -r M-h               resize-pane -L 5
#no-r# bind -T prefix       -r M-j               resize-pane -D 5
bind -T prefix       -r M-j               resize-pane -D 5
#no-r# bind -T prefix       -r M-k               resize-pane -U 5
bind -T prefix       -r M-k               resize-pane -U 5
#no-r# bind -T prefix       -r M-l               resize-pane -R 5
bind -T prefix       -r M-l               resize-pane -R 5
#no-r# bind -T prefix       -r M-H               resize-pane -L
bind -T prefix       -r M-H               resize-pane -L
#no-r# bind -T prefix       -r M-J               resize-pane -D
bind -T prefix       -r M-J               resize-pane -D
#no-r# bind -T prefix       -r M-K               resize-pane -U
bind -T prefix       -r M-K               resize-pane -U
#no-r# bind -T prefix       -r M-L               resize-pane -R
bind -T prefix       -r M-L               resize-pane -R

# scroll in prefix
bind -T prefix          M-K               { copy-mode ; send -X scroll-up }
bind -T prefix          M-k               { copy-mode ; send -X halfpage-up }
bind -T prefix          M-J               { copy-mode ; send -X scroll-down }
bind -T prefix          M-j               { copy-mode ; send -X halfpage-down }

# repeatable switch
#no-r# bind -T prefix       -r p                 previous-window
bind -T prefix          p                 previous-window
#no-r# bind -T prefix       -r n                 next-window
bind -T prefix          n                 next-window
#no-r# bind -T prefix       -r o                 select-pane -t :.+
bind -T prefix          o                 select-pane -t :.+
#no-r# bind -T prefix       -r O                 select-pane -t :.-
bind -T prefix          O                 select-pane -t :.-
#no-r# bind -T prefix       -r (                 switch-client -p
bind -T prefix          (                 switch-client -p
#no-r# bind -T prefix       -r )                 switch-client -n
bind -T prefix          )                 switch-client -n

# repeatable switch last
#no-r# bind -T prefix       -r M-\'              switch-client -l
bind -T prefix          M-\'              switch-client -l
#no-r# bind -T prefix       -r M-\;              switch-client -l
bind -T prefix          M-\;              switch-client -l
#no-r# bind -T prefix       -r \;                last-window
bind -T prefix          \;                last-window
#no-r# bind -T prefix       -r \'                last-pane
bind -T prefix          \'                last-pane
#no-r# #ind -T prefix       -r l                 last-window
#ind -T prefix          l                 last-window
#no-r# #ind -T prefix       -r L                 switch-client -l
#ind -T prefix          L                 switch-client -l

# swap window
#no-r# bind -T prefix       -r P                 run "#{q:@tmux}/sh swap-window prev #{q:window_id}"
bind -T prefix          P                 run "#{q:@tmux}/sh swap-window prev #{q:window_id}"
#no-r# bind -T prefix       -r N                 run "#{q:@tmux}/sh swap-window next #{q:window_id}"
bind -T prefix          N                 run "#{q:@tmux}/sh swap-window next #{q:window_id}"

# mark window (make repeatable)
#no-r# bind -T prefix       -r m                 select-pane -m
bind -T prefix          m                 select-pane -m
#no-r# bind -T prefix       -r M                 select-pane -M
bind -T prefix          M                 select-pane -M

# less pane
#ind -T prefix          \~                new-window "tmux capture-pane -S - -E - -aepJt {last} | less -SRi +G"
#ind -T prefix          \~                new-window 'tmux capture-pane -S - -E -  -epJt {last} | less -SRi +G'
# Nvim pane
bind -T prefix          M-\~              run "#{q:@tmux}/sh nview :#{q:window_width} :#{q:window_height} -1000 1300"
bind -T prefix          M-`               run "#{q:@tmux}/sh nview :#{q:window_width} :#{q:window_height}"

# removing -N from default list-keys binding
bind -T prefix          ?                 list-keys
bind -T prefix          /                 command-prompt -k -p key "list-keys -1 \"%%%\""

# clear alerts
bind -T prefix          a                 kill-session -C

# toggle line wrap
bind -T prefix          W                 { set -wu window-size ; resize-window -x 1000 }
bind -T prefix          M-w               run "#{q:@tmux}/sh interactive-resize-window #{q:window_width}"
bind -T prefix          C-w               set -wu window-size

# select window by number
#no-r# bind -T prefix       -r 0                 select-window -t 0
bind -T prefix          0                 select-window -t 0
#no-r# bind -T prefix       -r 1                 select-window -t 1
bind -T prefix          1                 select-window -t 1
#no-r# bind -T prefix       -r 2                 select-window -t 2
bind -T prefix          2                 select-window -t 2
#no-r# bind -T prefix       -r 3                 select-window -t 3
bind -T prefix          3                 select-window -t 3
#no-r# bind -T prefix       -r 4                 select-window -t 4
bind -T prefix          4                 select-window -t 4
#no-r# bind -T prefix       -r 5                 select-window -t 5
bind -T prefix          5                 select-window -t 5
#no-r# bind -T prefix       -r 6                 select-window -t 6
bind -T prefix          6                 select-window -t 6
#no-r# bind -T prefix       -r 7                 select-window -t 7
bind -T prefix          7                 select-window -t 7
#no-r# bind -T prefix       -r 8                 select-window -t 8
bind -T prefix          8                 select-window -t 8
#no-r# bind -T prefix       -r 9                 select-window -t 9
bind -T prefix          9                 select-window -t 9

# choose-tree
bind -T prefix          w                 'choose-tree -Zw -O time'
bind -T prefix          s                 'choose-tree -Zs -O time'

# rlwrap
bind -T prefix          r                 run "#{q:@tmux}/sh rlwrap .#{pane_id} .#{@rlwrapper} .#{@rlwrappee}"

# swap pane (repeatable)
#no-r# bind -T prefix       -r \{                swap-pane -U
bind -T prefix          \{                swap-pane -U
#no-r# bind -T prefix       -r \}                swap-pane -D
bind -T prefix          \}                swap-pane -D

# mode-out
bind -T prefix          c                 'new-window ; run "#{q:@tmux}/sh mode-out"'

# mode-out
bind -T prefix          BSpace            display-panes -d 2000

# fakezoom
bind -T prefix          \\                resize-pane -Z
bind -T prefix          \|                run "#{q:@tmux}/sh fakezoom"

# rotate
#no-r# bind -T prefix       -r M-[               rotate-window -U
bind -T prefix          M-[               rotate-window -U
#no-r# bind -T prefix       -r M-]               rotate-window -D
bind -T prefix          M-]               rotate-window -D

# Reload
#ind -T prefix          R                 run "#{q:@tmux}/sh reset && tmux display RESET"
#ind -T prefix          r                 run "#{q:@tmux}/sh reset && tmux display RESET"
bind -T prefix          M-r               run "#{q:@tmux}/sh reset && tmux display RESET"
bind -T prefix          M-R               run "#{q:@tmux}/sh init  && tmux display INIT"
