set -g @modal  1
set -g prefix  None
set -g prefix2 None


%if "#{==:#{session_id},}"
    set -g @mode 0
%else
    run "#{q:@tmux}/sh mode-out"
%endif


bind -T root            M-`               send `
bind -T root            `                 run "#{q:@tmux}/sh mode-in"
#no-r# bind -T copy-mode-vi -r `                 run "#{q:@tmux}/sh mode-in"
bind -T copy-mode-vi    `                 run "#{q:@tmux}/sh mode-in"
#no-r# bind -T copy-mode    -r `                 run "#{q:@tmux}/sh mode-in"
bind -T copy-mode       `                 run "#{q:@tmux}/sh mode-in"
bind                    `                 'send ` ; run "#{q:@tmux}/sh mode-out"'
#no-r# #ind                 -r Enter             run "#{q:@tmux}/sh mode-out"
#ind                    Enter             run "#{q:@tmux}/sh mode-out"
#no-r# bind                 -r Space             run "#{q:@tmux}/sh mode-out"
bind                    Space             run "#{q:@tmux}/sh mode-out"
#no-r# bind                 -r Escape            run "#{q:@tmux}/sh mode-out return"
bind                    Escape            run "#{q:@tmux}/sh mode-out return"
#no-r# bind                 -r C-c               run "#{q:@tmux}/sh mode-out return"
bind                    C-c               run "#{q:@tmux}/sh mode-out return"
bind -T copy-mode-vi    MouseDragEnd1Pane send Enter
bind -T copy-mode       MouseDragEnd1Pane send Enter

#no-r# bind                 -r Any               run "#{q:@tmux}/sh beep #{q:client_tty}"
bind                    Any               run "#{q:@tmux}/sh beep #{q:client_tty}"
bind -T copy-mode-vi    Any               run "#{q:@tmux}/sh beep #{q:client_tty}"
bind -T copy-mode       Any               run "#{q:@tmux}/sh beep #{q:client_tty}"


set-hook -ga client-session-changed 'run "#{q:@tmux}/sh client-session-changed"'

set-hook -ga client-detached        'run "#{q:@tmux}/sh client-detached-modal #{q:session_id} #{q:session_many_attached}"'
set-hook -ga pane-mode-changed      'run "#{q:@tmux}/sh pane-mode-changed-modal"'

# set-hook -ga after-unbind-key       'run "#{q:@tmux}/sh after-unbind-key-modal #{q:@mode-rebind}"'
set-hook -ga after-bind-key         'run "#{q:@tmux}/sh after-bind-key-modal #{q:@mode-rebind}"'
